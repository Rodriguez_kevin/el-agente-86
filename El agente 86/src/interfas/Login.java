package interfas;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField usuario;
	private JPasswordField contraseņa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		
		try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
 
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setTitle("El Agente 86");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 305, 419);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnLogin = new JButton("Log in >");
		
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnLogin.setForeground(new Color(255, 255, 255));
		btnLogin.setBackground(new Color(252, 116, 9 ));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String user = usuario.getText();
				String pass = contraseņa.getText();
				validarUsuario(user, pass);
			}
		});
		btnLogin.setBounds(4, 305, 288, 75);
		contentPane.add(btnLogin);
		
		usuario = new JTextField();
		usuario.setBounds(76, 192, 186, 31);
		contentPane.add(usuario);
		usuario.setColumns(10);
		
		contraseņa = new JPasswordField();
		contraseņa.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
				String user = usuario.getText();
				String pass = contraseņa.getText();
				
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					if(validarUsuario(user, pass))
						iniciarSesion();
					else 
						JOptionPane.showMessageDialog(null, "Contraseņa o usuario incorrectos");
					
				}
			}
		});
		contraseņa.setBounds(76, 238, 186, 31);
		contentPane.add(contraseņa);
		
		JLabel titulo = new JLabel("Generador de Redes");
		titulo.setForeground(new Color(255, 255, 255));
		titulo.setToolTipText("");
		
		titulo.setFont(new Font("Tahoma", Font.BOLD, 25));
		titulo.setBounds(20, 53, 267, 79);
		contentPane.add(titulo);
		
		JLabel label_1 = new JLabel("");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		label_1.setIcon(new ImageIcon(Login.class.getResource("/imagenes/imagen_login.jpg")));
		label_1.setBounds(0, 0, 300, 395);
		contentPane.add(label_1);
	}
	public boolean validarUsuario(String user, String pass) {
		return user.equals("Rodriguez_Diaz") && pass.equals("programaciontres");
			
	}
	public void iniciarSesion() {
		Mapa mapa = new Mapa();
		mapa.frame.setVisible(true);
		mapa.frame.setLocationRelativeTo(null);	
		dispose();
			
		}
		
		
	}

