package interfas;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.TextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import logica.*;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import org.openstreetmap.gui.jmapviewer.JMapViewer.ZOOM_BUTTON_STYLE;

public class Mapa {
	private ArrayList<Agente> _agentes;
	private Grafo _arbolAGM;
	public JFrame frame;
	private int _ID =0;
	private JMapViewer _mapa;
	private TextArea _areaDeInfo;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		try {
            //here you can put the selected theme class name in JTattoo
            UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
 
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Mapa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		
		
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mapa window = new Mapa();
					window.frame.setTitle("El Agente 86");
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mapa() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		
		int alto = 750;
		int ancho =1000;
		
		frame = new JFrame();
		frame.setBounds(100, 100,ancho ,alto );
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_areaDeInfo = new TextArea("");
		_agentes=new ArrayList<Agente>();
		frame.getContentPane().setLayout(null);
		_mapa= new JMapViewer();
		_mapa.setBounds(0, 0, 995, 711);
		_mapa.setZoomButtonStyle(ZOOM_BUTTON_STYLE.HORIZONTAL);
		frame.getContentPane().add(_mapa);
		_mapa.setLayout(null);
		
		_mapa.addMouseListener(new MouseAdapter() 
		{
			public void mouseClicked(MouseEvent e) 
			{
				Coordinate coord = (Coordinate) _mapa.getPosition(e.getPoint());
				String nombre= JOptionPane.showInputDialog("ingrese nombre del agente");
				MapMarker punto=new MapMarkerDot(nombre, coord);
				_mapa.addMapMarker(punto);
				agregarAgente(coord,nombre,_ID);
				_ID++;
			}
	    });
		
		JButton btnGenerarRed = new JButton("Generar Red");
		btnGenerarRed.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if(_agentes.size()<2) {
					JOptionPane.showMessageDialog(frame, "debe agregar almenos dos marcadores");;
				}
				generarAGM();
				informacionDelAGM();
			}
		});
		btnGenerarRed.setBounds(859, 652, 101, 23);
		btnGenerarRed.setForeground(new Color(255, 255, 255));
		btnGenerarRed.setBackground(new Color(252, 116, 9 ));
		_mapa.add(btnGenerarRed);
		
		JButton btnNuevoMapa = new JButton("Nuevo Mapa");
		btnNuevoMapa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				nuevoMapa();
			}
		});
		btnNuevoMapa.setBounds(712, 652, 101, 23);
		btnNuevoMapa.setForeground(new Color(255, 255, 255));
		btnNuevoMapa.setBackground(new Color(252, 116, 9 ));
		_mapa.add(btnNuevoMapa);
		
		JButton btnCerrarSecion = new JButton("Cerrar Sesion");
		btnCerrarSecion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Login log= new Login();
				log.setVisible(true);
				log.setLocationRelativeTo(null);
				frame.dispose();
			}
		});
		btnCerrarSecion.setBounds(848, 11, 111, 23);
		btnCerrarSecion.setForeground(new Color(255, 255, 255));
		btnCerrarSecion.setBackground(new Color(252, 116, 9 ));
		_mapa.add(btnCerrarSecion);
		
	}

	public void agregarAgente(Coordinate coord, String nombre, int id)
	{
		Agente age=new Agente(coord.getLat(),coord.getLon(),nombre,id);
		_agentes.add(age);
	}
	public void generarAGM() {
		_arbolAGM= new Grafo(_agentes);
		graficarAGM(_arbolAGM.generarAGM());
		
	
	}
	
	public void graficarAGM(Double[][] matrizAGM) {
		
		for (int i = 0; i < matrizAGM.length; i++) 
		for (int j = 0; j < matrizAGM.length; j++) 
			if(matrizAGM[i][j]!=null) 
				graficarLinea(i,j);
		}
	
	public void graficarLinea(int a, int b) {
		ArrayList<Coordinate> lista = new  ArrayList<Coordinate>();
		double latA= _agentes.get(a).getPosX();
		double lonA= _agentes.get(a).getPosY();
		double latB= _agentes.get(b).getPosX();
		double lonB= _agentes.get(b).getPosY();
		Coordinate one = new Coordinate(latA,lonA);
		Coordinate two = new Coordinate(latB,lonB);
		lista.add(one);
		lista.add(two);
		lista.add(two);
		_mapa.addMapPolygon(new MapPolygonImpl(lista));
	}
	
	private void nuevoMapa() {
		
		_mapa.removeAllMapMarkers();
		_mapa.removeAllMapPolygons();
		_areaDeInfo.setText(null);
		_areaDeInfo.setVisible(false);
		_agentes.clear();
	}
	
	private void informacionDelAGM() {
		_areaDeInfo.setVisible(true);
		_areaDeInfo.setBounds(10, 500, 200, 222);
		_areaDeInfo.setEditable(false);
		_areaDeInfo.setCursor(null);  
		_areaDeInfo.setFocusable(false);
		String msj= "La arista Menor es:\n"+ _arbolAGM.aristaMenorTotal()+ 
				"\n\nLa arista Mayor es:\n"+ _arbolAGM.aristaMayorTotal()+
				"\n\nLa distancia Total de la red es: "+_arbolAGM.distanciaTotal()+" km";
		
		_areaDeInfo.setText(msj);
		_mapa.add(_areaDeInfo);
	}
}
























