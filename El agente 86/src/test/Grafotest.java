package test;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Point;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.jupiter.api.Test;


import logica.*;

class Grafotest {
	
	Grafo grafo= new Grafo(inicio());
	Double[][] matriz= {{null    ,1737.842,2355.665},
						{1737.842,null    ,1577.375},
						{2355.665,1577.375,null    }};
	
	
	AGM agm= new AGM(grafo.get_matrizAGM(),grafo.get_matrizDistancias());
	@Before
	ArrayList<Agente> inicio(){
		ArrayList<Agente> agentes= new ArrayList<Agente>();
		Agente age1 = new Agente(40.979898069620134, -4.74609375,"maria",1);
		Agente age2 = new Agente(49.83798245308484, 13.7109375,"luis",2);
		Agente age3 = new Agente(37.02009820136811, 22.1484375,"laura",3);
		agentes.add(age1);agentes.add(age2);agentes.add(age3);
		return agentes;
	}
	
	@Test
	void menorDeColumnaTest() {
		boolean[] estados= {true,false,false};
		int filaMenor= agm.menorDeColumna(0, estados, matriz);
		assertEquals(1, filaMenor);
	}
	@Test
	void menorDeColumna2Test() {
		boolean[] estados= {true,true,true};
		int filaMenor= agm.menorDeColumna(0, estados, matriz);
		assertEquals(-1, filaMenor);
	}
	@Test
	void menorDeColumna3Test() {
		boolean[] estados= {true,true,false};
		int filaMenor= agm.menorDeColumna(0, estados, matriz);
		assertEquals(2, filaMenor);
	}
	@Test
	void medirDistaciaTest() {
		double distancia = grafo.medirDistancia(inicio().get(0), inicio().get(1));
		assertEquals(1737.842, distancia);
	}
	@Test
	void medirDistacia2Test() {
		double distancia = grafo.medirDistancia(inicio().get(1), inicio().get(0));
		assertEquals(1737.842, distancia);
	}
	@Test
	void aristaMenorTest() {
		boolean[] estados= {true,true,false};
		Point respuesta= agm.aristaMenor(estados,matriz);
		Point esperado= new Point(2,1); 
		assertEquals(esperado, respuesta);
	}
	@Test
	void distanciaTotalTest() {
		grafo.generarAGM();
		double esperado= 1737.842+1577.375;
		double distancia= grafo.distanciaTotal();
		assertEquals(esperado,distancia);
	}
	@Test
	void aristaMenorTotalTest() {
		grafo.generarAGM();
		Point esperado= new Point(1,2);
		Point AristaMenor = grafo.aristaMenorDelAGM();
		assertEquals(esperado,AristaMenor);
	}
	@Test
	void aristaMayorTotalTest() {
		grafo.generarAGM();
		Point esperado= new Point(0,1);
		Point AristaMayor = grafo.aristaMayorDelAGM();
		assertEquals(esperado,AristaMayor);
	}
	@Before
	ArrayList<Double> matricesAgm() {
		Double[][] matrizAGM= grafo.generarAGM();
		ArrayList<Double> matrizagm= new ArrayList<Double>();
		matrizagm.add(matrizAGM[0][1]);
		matrizagm.add(matrizAGM[1][0]);
		matrizagm.add(matrizAGM[1][2]);
		matrizagm.add(matrizAGM[2][1]);
		return matrizagm;
	}
	@Before
	ArrayList<Double> matricesEsperado() {
		ArrayList<Double> matrizEsperada= new ArrayList<Double>();
		matrizEsperada.add(matriz[0][1]);
		matrizEsperada.add(matriz[1][0]);
		matrizEsperada.add(matriz[1][2]);
		matrizEsperada.add(matriz[2][1]);
		return matrizEsperada;
	}
	@Test
	void AGMtest() {
		ArrayList<Double> matrizEsperada= matricesEsperado();
		ArrayList<Double> matrizagm = matricesAgm();
		
		assertEquals(matrizEsperada,matrizagm);
	}

}




















