package logica;

import java.awt.Point;
import java.util.ArrayList;

public class AGM {
	private Double[][] _matrizAGM;
	private Double[][] _matrizDistancias;
	private ArrayList<Double> _distanciasAgregadas;
	boolean[] verticesAgregados;
	
	public AGM(Double[][] matrizAGM,Double[][] matrizDistancias) {
		this._matrizAGM=matrizAGM;
		this._matrizDistancias=matrizDistancias;
		this._distanciasAgregadas= new ArrayList<Double>() ;
	}
	
	
	public Double[][] iniciarAGM()
	{
		inicializarMatriz(_matrizAGM);
		verticesAgregados= new boolean[_matrizDistancias.length];
		verticesAgregados[0]=true;
		while(!TodosMarcados(verticesAgregados))
		{
			Point arista= aristaMenor(verticesAgregados,_matrizDistancias);
			_distanciasAgregadas.add(_matrizDistancias[arista.x][arista.y]);
			_matrizAGM[arista.x][arista.y]= _matrizDistancias[arista.x][arista.y];
			_matrizAGM[arista.y][arista.x]= _matrizDistancias[arista.y][arista.x];
			_matrizDistancias[arista.x][arista.y]=null;
			_matrizDistancias[arista.y][arista.x]=null;
			if (!verticesAgregados[arista.x]) 
				verticesAgregados[arista.x]=true;
		}
		return _matrizAGM;
	}
	
	public Point aristaMenor(boolean[] verticesAgregados,Double[][] matrizDistancias) 
	{
		Point arista= new Point(-1,-1);
		for (int i = 0; i < verticesAgregados.length; i++) 
			{
				if(verticesAgregados[i]) 
				{
					Integer columna = i;
					Integer fila = menorDeColumna(i,verticesAgregados,matrizDistancias);
					
					if (arista.x==-1 && arista.y==-1) 
					{
						arista.x=fila;
						arista.y=columna;
					}
					else 
						if(fila!=-1) 
							if(matrizDistancias[fila][columna] < matrizDistancias[arista.x][arista.y]) 
							{
								arista.x=fila;
								arista.y=columna;
							}
				}
			}
		
		return arista;
	}
	
	public int menorDeColumna(Integer columna, boolean[] verticesAgregados, Double[][] matrizDistancias) 
	{
		int punto=-1;
		for (int i = 0; i < matrizDistancias.length; i++) 
		{
			if(matrizDistancias[columna][i] != null) 
			{
				if (!verticesAgregados[i]) 
				{
					if(punto==-1)
					{ 
						punto=i;
					}
					else 
					{
						if (matrizDistancias[columna][i] < matrizDistancias[columna][punto]) 
							punto=i;
					}
				}
			}
		}
		return punto;
	}
	
	private void inicializarMatriz(Double[][] mtx) 
	{
		for (int i = 0; i < mtx.length; i++) {
			for (int j = 0; j < mtx.length; j++) {
				mtx[i][j]=null;
			}
		}
	}
		
	private boolean TodosMarcados(boolean[] vertice)
	{ 
        for (boolean b : vertice) 
            if (!b) 
                return b;
            
       return true;
    }


	public Double[][] get_matrizAGM() {
		return _matrizAGM;
	}


	public void set_matrizAGM(Double[][] _matrizAGM) {
		this._matrizAGM = _matrizAGM;
	}


	public Double[][] get_matrizDistancias() {
		return _matrizDistancias;
	}


	public void set_matrizDistancias(Double[][] _matrizDistancias) {
		this._matrizDistancias = _matrizDistancias;
	}


	public ArrayList<Double> get_distanciasAgregadas() {
		return _distanciasAgregadas;
	}


	public void set_distanciasAgregadas(ArrayList<Double> _distanciasAgregadas) {
		this._distanciasAgregadas = _distanciasAgregadas;
	}
	

}
