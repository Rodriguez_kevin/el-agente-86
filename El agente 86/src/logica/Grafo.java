package logica;
import java.awt.Point;
import java.util.ArrayList;

public class Grafo {
	private  ArrayList<Agente> _agentes;
	private  Double[][] _matrizDistancias;
	private  Double[][] _matrizAGM;
	private  AGM arbolAGM;
	
	public Grafo(ArrayList<Agente> agentes)
	{
		this._agentes = agentes;
		this._matrizDistancias = new Double[agentes.size()][agentes.size()];
		this._matrizAGM= new Double[agentes.size()][agentes.size()];
		cargarMatriz();
	}
	
	private void cargarMatriz() 
	{
		for (int i = 0; i < _matrizDistancias.length; i++) 
		{
			for (int j = 0; j < _matrizDistancias.length; j++) 
			{
				if(i==j)
					_matrizDistancias[i][j]=null;
				
				else {
					double distancia=medirDistancia(_agentes.get(i),_agentes.get(j));
					
					_matrizDistancias[i][j]=distancia;
					
					}
			}
		}
	}
	
	public double medirDistancia(Agente age1, Agente age2 )
	{
		double distanciaEnMetros=distanciaMetros(age1.getPosX(),age1.getPosY(),age2.getPosX(),age2.getPosY());
		double distanciaEnKm= distanciaEnMetros/1000.0;
		return distanciaEnKm;
	}
	
	private double distanciaMetros(double lat1, double lng1, double lat2, double lng2) 
	{
		double l1 = Math.toRadians(lat1);
	    double l2 = Math.toRadians(lat2);
	    double g1 = Math.toRadians(lng1);
	    double g2 = Math.toRadians(lng2);
	    double dist = Math.acos(Math.sin(l1) * Math.sin(l2) + Math.cos(l1) * Math.cos(l2) * Math.cos(g1 - g2));
	    if(dist < 0) 
	        dist = dist + Math.PI;
	    
	    return Math.round(dist * 6371000);
	}
	
	public Double[][] generarAGM() {
		arbolAGM = new AGM(_matrizAGM, _matrizDistancias);
		_matrizAGM = arbolAGM.iniciarAGM();
		return _matrizAGM;
	}
	
	public String aristaMenorTotal() {
		Point coordArista= aristaMenorDelAGM();
		String age1= _agentes.get(coordArista.x).getNombre();
		String age2= _agentes.get(coordArista.y).getNombre();
		
		return"desde: "+age1+" a: "+ age2+ " con distancia de: "+ _matrizAGM[coordArista.x][coordArista.y]+" km";
		
	}
	public Point aristaMenorDelAGM() {
		Point arista= new Point(-1,-1);
		for (int fila = 0; fila < _matrizAGM.length; fila++) {
		for (int columna = 0; columna < _matrizAGM.length; columna++) {
			if (_matrizAGM[fila][columna]!=null) { 
				if(arista.x==-1 && arista.y==-1) {
					arista.x=fila;
					arista.y=columna;
				}
				else
					if(_matrizAGM[fila][columna] < _matrizAGM[arista.x][arista.y]) 
					{
						arista.x=fila;
						arista.y=columna;
					}
			}
		}
		}
		return arista;
	}
	
	public String aristaMayorTotal() {
		Point coordArista= aristaMayorDelAGM();
		String age1= _agentes.get(coordArista.x).getNombre();
		String age2= _agentes.get(coordArista.y).getNombre();
		return"desde: "+age1+" a: "+ age2+ " con distancia de: "+ _matrizAGM[coordArista.x][coordArista.y]+" km";
	}
	public Point aristaMayorDelAGM() {
		Point arista= new Point(-1,-1);
		for (int fila = 0; fila < _matrizAGM.length; fila++) {
		for (int columna = 0; columna < _matrizAGM.length; columna++) {
			if (_matrizAGM[fila][columna]!=null) 
			{ 
				if(arista.x==-1 && arista.y==-1) 
				{
					arista.x=fila;
					arista.y=columna;
				}
				else
					if(_matrizAGM[fila][columna] > _matrizAGM[arista.x][arista.y]) 
					{
						arista.x=fila;
						arista.y=columna;
					}
			}
		}
		}
		return arista;
	}
	
	public double distanciaTotal() {
		double suma=0;
		for (Double  distancia : arbolAGM.get_distanciasAgregadas()) {
			suma+=distancia;
		}
		return suma;
	}

	public ArrayList<Agente> get_agentes() {
		return (ArrayList<Agente>) _agentes.clone();
	}

	public void set_agentes(ArrayList<Agente> _agentes) {
		this._agentes = _agentes;
	}

	public Double[][] get_matrizDistancias() {
		return _matrizDistancias.clone();
	}

	public void set_matrizDistancias(Double[][] _matrizDistancias) {
		this._matrizDistancias = _matrizDistancias;
	}

	public Double[][] get_matrizAGM() {
		return _matrizAGM.clone();
	}

	public void set_matrizAGM(Double[][] _matrizAGM) {
		this._matrizAGM = _matrizAGM;
	}

	public ArrayList<Double> getDistanciasAgregadas() {
		return arbolAGM.get_distanciasAgregadas();
	}
	
	
}
