package logica;

public class Agente {
	private double posX;
	private double posY;
	private String nombre;
	private int ID;
	
	public Agente(double posX, double posY, String nombre,int ID){
		this.posX=posX;
		this.posY= posY;
		this.nombre=nombre;
		this.ID=ID;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

}
